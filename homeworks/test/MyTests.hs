-- Your tests go here

module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
myTests = [ testGroup "HWs"
             [ testCase "Works on Alice" $ hw0_0 "Alice" @?= "Hello, Alice" ]
         ,
	testGroup "Sem eval" [
                       testCase "eval (\\x . x) y == y" $ myEval (Apply (Lam "x" (Var "x")) (Var "y")) @?= Just (Var "y"),
	               
		       testCase "eval (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)" $ myEval (Apply (Lam "x" (Lam "y" (Apply (Var "x") (Var "y")))) (Lam "z" (Var "z"))) @?= Just (Lam "y" (Apply (Lam "z" (Var "z")) (Var "y"))),

                       testCase "eval (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\y . (\\z . z) y) (\\u . \\v . v)" $ show (myEval (Apply (Apply (Lam "x" (Lam "y" (Apply (Var "x") (Var "y"))))(Lam "z" (Var "z")))(Lam "u" (Lam "v" (Var "v"))))) @?= "Just (\\y . (\\z . z) y) (\\u . (\\v . v))",
                       testCase "eval (\\x . (\\x . x) x) == (\\x . (\\x . x) x)" $ show (myEval (Lam "x" (Apply (Lam "x" (Var "x")) (Var "x")))) @?= "Just (\\x . (\\x . x) x)",
                       
		       testCase "eval (\\x . \\y . x) y = (\\a . y)" $ show (myEval (Apply (Lam "x" (Lam "y" (Var "x"))) (Var "y"))) @?= "Just (\\a . y)",
                       testCase "eval (\\x . \\y . \\z . x y) y = (\\a . \\x . x)" $ show (myEval (Apply (Lam "x" (Lam "y" (Lam "z" (Apply (Var "x") (Var "y"))))) (Var "y"))) @?= "Just (\\a . (\\z . y a))"
                 ],

            testGroup "Sem eval'" [

                       testCase "eval' (\\x . x) y == y" $ myEval' (Apply (Lam "x" (Var "x")) (Var "y")) @?= Var "y",

                       testCase "eval' (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)" $ myEval' (Apply (Lam "x" (Lam "y" (Apply (Var "x") (Var "y"))))(Lam "z" (Var "z"))) @?= Lam "y" (Apply (Lam "z" (Var "z")) (Var "y")),
                       
		       testCase "eval' (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\u . \\v . v)" $ myEval' (Apply (Apply (Lam "x" (Lam "y" (Apply (Var "x") (Var "y"))))(Lam "z" (Var "z")))(Lam "u" (Lam "v" (Var "v")))) @?= Lam "u" (Lam "v" (Var "v")),

                       testCase "eval' (\\x . (\\x . x) x) == (\\x . (\\x . x) x)" $ show (myEval' (Lam "x" (Apply (Lam "x" (Var "x")) (Var "x")))) @?= "(\\x . (\\x . x) x)",

                       testCase "eval' (\\x . \\y . x) y = (\\a . y)" $ show (myEval' (Apply (Lam "x" (Lam "y" (Var "x"))) (Var "y"))) @?= "(\\a . y)",

                       testCase "eval' (\\x . \\a . x a) a = (\\b . a b)" $ show (myEval' (Apply (Lam "x" (Lam "a" (Apply (Var "x") (Var "a")))) (Var "a"))) @?= "(\\b . a b)",

                       testCase "eval' (\\x . \\y . \\z . x y) y = (\\a . \\z . y a)" $ show (myEval' (Apply (Lam "x" (Lam "y" (Lam "z" (Apply (Var "x") (Var "y"))))) (Var "y"))) @?= "(\\a . (\\z . y a))"

                 ],

	   testGroup "Problem 41" [
                       testCase "largest n-digit pandigital prime is 7652413" $ pandigital_prime @?= 7652413
		 ]
            ]

