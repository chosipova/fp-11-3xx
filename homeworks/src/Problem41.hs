--We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is also prime.

--What is the largest n-digit pandigital prime that exists?


module Problem41
	(
	primeD,
	pandigital_prime
	)where

import Data.Char (intToDigit)
import Data.List

primeD :: Integer -> Bool
primeD 1 = False
primeD 2 = True
primeD p | p <= 0 = False        
          | p `mod` 2 == 0 = False 
          | otherwise = primeD' p 3
primeD' p x | p < x*x = True
             | p `mod` x == 0 = False
             | otherwise = primeD' p (x+2)

pandigital_prime = maximum [ n' | d <- [3..9], n <- permute ['1'..intToDigit d],
                            let n' = read n, primeD n']
    where
        permute "" = [""]
        permute str = [(x:xs)| x <- str, xs <- permute (delete x str)]

