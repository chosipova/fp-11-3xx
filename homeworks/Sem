module Sem where

import Data.Char

data MySemTerm = Var String
        | Lam String MySemTerm   -- String а не term
        | Apply MySemTerm MySemTerm
        deriving Eq

instance Show MySemTerm where
    show (Var v) = v
    show (Lam v t) = "(\\" ++ v ++ " . " ++ show t ++ ")"
    show (Apply t1 t2) = show t1 ++ " " ++ show t2

--Вычисление терма на один шаг

myEval :: MySemTerm -> Maybe MySemTerm
myEval term@(Apply app@(Apply t1 t2) t3) = Just $ Apply myEval_app t3
    where (Just myEval_app) = myEval app
myEval term@(Apply t1 app@(Apply t2 t3)) = Just $ Apply t1 myEval_app
    where (Just myEval_app) = myEval app
myEval term@(Apply lam1@(Lam v1 t1) l2) = Just (swap v1 l2 t1)
myEval term = Just term

swap :: String -> MySemTerm -> MySemTerm -> MySemTerm
swap var term inMySemTerm = case inMySemTerm of
    Var v -> if v == var 
                    then term 
                    else Var v
    lam@(Lam v (Var v1)) -> if v /= v1 && v1 == var 
                    then (                 
                        if Var v == term then
                        Lam (new_val v) term  
                        else Lam v term )
                    else lam 
    lam@(Lam v (Lam v2 t)) -> if v == var || v2 == var
                    then lam
                    else if term == Var v
                    then  Lam (new_val v) (Lam v2 (swap var term (swap v (Var $ new_val v) t )))
                    else if term == Var v2 
                    then Lam v (Lam (new_val v2) (swap var term (swap v2 (Var $ new_val v2) t )))
                    else Lam v (Lam v2 (swap var term t))
    lam@(Lam v (Apply t1 t2)) -> if v == var
                    then lam
                    else if term == Var v then Lam (new_val v) (swap var term (swap v (Var $ new_val v) (Apply t1 t2)))
                        else Lam v (swap var term (Apply t1 t2))

    app@(Apply t1 t2) -> Apply (swap var term t1) (swap var term t2)
    where new_val v = (swapToFreeVariable [v] "a")          

getRelatedVariables :: MySemTerm -> [String]
getRelatedVariables term = case term of
    Lam v t -> v : getRelatedVariables t
    Apply t1 t2 -> getRelatedVariables t1 ++ getRelatedVariables t2
    (Var v) -> [v]

swapToFreeVariable :: [String] -> String -> String
swapToFreeVariable usedVars var = if var `elem` usedVars
                            then swapToFreeVariable usedVars (nextVar var)
                            else var
                 where nextVar [ch] = [chr (ord ch + 1)]

-- Вычисление на много шагов

myEval' :: MySemTerm -> MySemTerm
myEval' term@(Apply app@(Apply t1 t2) t3) = myEval' $ Apply (myEval' app) t3
myEval' term@(Apply t1 app@(Apply t2 t3)) = myEval' $ Apply t1 (myEval' app)
myEval' term@(Apply lam1@(Lam v1 t1) l2) = myEval' $ swap v1 l2 t1
myEval' term = term
